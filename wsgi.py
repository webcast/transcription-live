# import eventlet

# eventlet.monkey_patch()

from app.client import app, socketio  # noqa

if __name__ == "__main__":
    socketio.run(app, port=8080, host="0.0.0.0")
