#!/bin/bash

echo 'Starting app...'
# We run the application using uwsgi
uwsgi --ini /projects/server/uwsgi.ini --mimefile /projects/etc/mime.types
