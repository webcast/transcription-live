import asyncio

from autobahn.asyncio.websocket import WebSocketServerFactory
from dotenv import load_dotenv

from app.server import MyServerProtocol
from app.utils import logger

load_dotenv()


if __name__ == "__main__":
    # Setting up a WebSocket server using asyncio for asynchronous I/O
    # and a WebSocketServerFactory to handle the WebSocket connections.
    logger.info("creating WebSocket server...")

    factory = WebSocketServerFactory()
    factory.protocol = MyServerProtocol

    loop = asyncio.get_event_loop()
    coro = loop.create_server(factory, "0.0.0.0", 8080)
    server = loop.run_until_complete(coro)
    logger.info("WebSocket server running...")
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.close()
