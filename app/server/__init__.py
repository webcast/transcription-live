import base64
import json
import os

import socketio  # type: ignore
from autobahn.asyncio.websocket import WebSocketServerProtocol  # type: ignore

from app.server_parts.data_manager import DataManager
from app.utils import logger


class MyServerProtocol(WebSocketServerProtocol):
    def __init__(self):
        super().__init__()
        self.room_id = None
        self.room_name = None
        self.room_password = None
        self.socketio_client = socketio.SimpleClient()
        self.socketio_client.connect(os.environ["SOCKETIO_SERVER_URL"])

    def onConnect(self, request):
        logger.info("Client connecting: {}".format(request.peer))

    def onOpen(self):
        logger.info("WebSocket connection open.")

    def onMessage(self, payload, isBinary):
        if isBinary:
            print("Binary message received: {} bytes".format(len(payload)))
        else:
            message = payload.decode("utf8")
            message_dict = json.loads(message)
            logger.debug(f"Text message received: {message_dict}")

            if "code" in message_dict:
                if message_dict["code"] == "OPEN":
                    self.handle_open(message_dict)
                elif (
                    message_dict["code"] == "HYP" or message_dict["code"] == "RES"
                ):  # Data
                    self.handle_message(message_dict)
                elif message_dict["code"] == "CLOSE":
                    self.handle_close()
                if message_dict["code"] == "END":
                    self.handle_close()

    def handle_close(self):
        logger.debug("Handling close event")
        if self.room_id:
            self.send_end()
            self.delete_room(self.room_id)
        else:
            logger.debug("Unable to send END message. Room ID is None")

    def handle_message(self, message_dict):
        # {'code': 'HYP', 'text-novar': '', 'text-var': 'the'}
        self.send_message(message_dict["code"], message_dict)

    def handle_open(self, message_dict: dict):
        # {"code":"OPEN", "channel": "1", "password" : "1234"}
        self.room_id = message_dict["channel"]
        self.room_name = f"Room {message_dict['channel']}"
        self.room_password = message_dict["password"]
        self.create_room(self.room_id, self.room_password, self.room_name)

        self.send_ready(
            message_dict["channel"],
            f"Room {message_dict['channel']}",
        )

    def onClose(self, wasClean, code, reason):
        self.handle_close()
        logger.info(
            f"WebSocket connection closed (onClose): {reason}. wasClean: {wasClean}"
        )

    def send_ready(self, room_id: str, room_name: str):
        """
        Sends a 'READY' message to the RTMP server.

        Returns:
            None
        """
        logger.info("Sending READY to RTMP server...")

        payload = {
            "code": "READY",
            "room_id": room_id,
            "room_name": room_name,
        }
        encoded_payload = json.dumps(payload).encode("utf-8")
        self.sendMessage(encoded_payload)

    def send_message(self, code: str, data: dict):
        logger.debug(f"Received code HYP or RES ({code})")
        message_type = "new_message"
        payload = {
            "channel": self.room_id,
            "code": code,
            "room_id": self.room_id,
            "name": f"Room #{self.room_id}",
            "text-novar": data["text-novar"],
            "text-var": data["text-var"],
        }
        logger.debug(f"Sending message ({message_type}) to SocketIO client: {payload}")
        self.socketio_client.emit(
            message_type,
            payload,
        )

    def send_end(self):
        # self.sendMessage(json.dumps({"code": "END"}).encode("utf-8"))
        message_type = "new_message"
        payload = {
            "channel": self.room_id,
            "code": "END",
            "room_id": self.room_id,
        }
        logger.debug(f"Sending message ({message_type}) to SocketIO client: {payload}")
        self.socketio_client.emit(
            message_type,
            payload,
        )

    def create_room(self, room_id: str, password: str, name: str):
        # Save the room in redis
        data_manager = DataManager()

        base64_password = base64.b64encode(bytes(password, "utf-8")).decode("utf-8")

        room_dict = {
            "password": base64_password,
            "name": name,
        }
        data_manager.add_room(room_id, room_dict)

    def delete_room(self, room_id: str):
        # Save the room in redis
        data_manager = DataManager()
        data_manager.delete_room(room_id)
