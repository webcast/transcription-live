import logging
import os
from datetime import datetime

from dotenv import load_dotenv
from pytz import timezone, utc


def zurich_time(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


load_dotenv()

if os.environ.get("DEBUG", "false") == "false":
    logging_level = logging.INFO
else:
    logging_level = logging.DEBUG

logging.Formatter.converter = zurich_time


logging.basicConfig(
    level=logging_level,
    format="%(asctime)s | %(levelname)s | %(name)s | %(message)s | %(filename)s:%(lineno)d",
)

logger = logging.getLogger("wsserver")
