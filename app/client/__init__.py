import os

from flask import Flask
from flask_cors import CORS
from flask_socketio import SocketIO, disconnect, emit, join_room, leave_room, send
from werkzeug.middleware.proxy_fix import ProxyFix

from app.server_parts.data_manager import DataManager, redis_url_with_pass
from app.utils import logger

logger.debug("Initialising Flask app")

app = Flask(__name__)
app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY", os.urandom(32))
CORS(app)

use_proxy = os.environ.get("USE_PROXY", "false") == "true"
debug_socketio = os.environ.get("DEBUG_SOCKETIO", "false") == "true"

if use_proxy:
    logger.debug("Using werkzeug proxy fix")
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1)  # type: ignore


socketio = SocketIO(
    app,
    cors_allowed_origins="*",
    message_queue=redis_url_with_pass,
    engineio_logger=debug_socketio,
)


@socketio.on("join")
def on_join(data):
    room_id = data["room_id"]
    password = data["password"]

    data_manager = DataManager()
    room = data_manager.get_room(room_id)
    if not room:
        logger.info("Room not found. Will disconnect")
        disconnect()
        return
    stored_password = room["password"]
    if stored_password != password:
        logger.info("Wrong password. Will disconnect")
        disconnect()
        return

    join_room(room_id)

    send(f"Joined the room {room_id}", to=room_id)


@socketio.on("leave")
def on_leave(data):
    room_id = data["room_id"]
    leave_room(room_id)
    send(f"Left the room {room_id}", to=room_id)


@socketio.on("connect")
def on_connect():
    logger.debug("Client connected")
    send("You are connected")
    emit("Hi there", {"data": "Connected"})


@socketio.on("disconnect")
def on_disconnect():
    logger.debug("Client disconnected")


@socketio.on("new_message")
def on_new_message(data):
    logger.debug("New message received!")
    logger.debug(f"Sending message: {data}")
    emit("new_message", {"data": data}, to=data["room_id"])


if __name__ == "__main__":
    socketio.run(app)


@app.route("/")
def hello():
    return "Hello World!"
