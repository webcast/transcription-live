import json
import os

import socketio
from autobahn.asyncio.websocket import WebSocketServerProtocol
from dotenv import load_dotenv

from app.server_parts.data_manager import DataManager
from app.utils import logger

load_dotenv()

sio = socketio.SimpleClient()
sio.connect(os.environ["SOCKETIO_SERVER_URL"])


class TranscriptionMasterBroadcastServer(WebSocketServerProtocol):
    N = 0

    # Estat
    WAITING = 0
    PROCESSING = 1
    LISTENING = 2

    # Mode

    MASTER = 0  # transcribe (retrieving audio data)

    RECOMNG = None  # To be initialized

    # Systems call content
    SYSCALL_CONTENT = None

    def __wrong_protocol(self):
        logger.warning(
            "Trying to communicate using an unknown protocol;"
            + " connection will be shutdown"
        )
        self.reset()
        self.sendClose()

    # Return True if format is ok.
    def __read_open_code(self, payload, isBinary):
        if isBinary:
            return False
        try:
            msg = json.loads(payload.decode("utf-8"))

            logger.info("Mesage: {}".format(msg))
            if msg["code"] == "OPEN":
                self.mode = TranscriptionMasterBroadcastServer.MASTER
            else:
                logger.info(msg["code"])
                return False
        except Exception as exc:
            logger.error(
                f"Error opening connection: missing parameters: {exc}", exc_info=True
            )
            return False
        return True

    # Return the code, if not present return None
    def __read_code(self, payload, isBinary):
        if isBinary:
            return None
        msg = json.loads(payload.decode("utf-8"))
        return msg.get("code")

    def __read_channel(self, payload, isBinary):
        if isBinary:
            return None
        msg = json.loads(payload.decode("utf-8"))
        return msg.get("channel")

    def __read_password(self, payload, isBinary):
        """
        Reads the password from the payload.

        Args:
            payload (bytes): The payload containing the password.
            isBinary (bool): Indicates whether the payload is binary or not.

        Returns:
            str: The password extracted from the payload, or None if the payload is binary.
        """
        if isBinary:
            return None
        msg = json.loads(payload.decode("utf-8"))
        return msg.get("password")

    def create_room(self, channel, password):
        """
        This method creates a new room in the TranscriptionMasterBroadcastServer.

        Parameters:
        channel (str): The channel name which will be used as the room_id.
        password (str): The password for the room.

        Returns:
        room_id (str): The ID of the created room.
        """
        # Log the start of room creation
        logger.info(f"Creating room for channel {channel}...")

        # Set the room_id to the provided channel name
        room_id = channel

        # Set the room_password to the provided password
        room_password = password

        # Increment the ROOM_COUNT of TranscriptionMasterBroadcastServer
        data_manager = DataManager()
        room_count = data_manager.get_room_count()
        room_count += 1
        data_manager.set_room_count(room_count)
        logger.debug(f"Room count: {room_count}")
        # Add the new room to the CURRENT_ROOMS dictionary of TranscriptionMasterBroadcastServer
        data_manager = DataManager()
        current_rooms = data_manager.get_data()

        logger.debug("Current rooms: {}".format(current_rooms))

        current_rooms[room_id] = {
            "id": room_id,
            "name": "Meeting #" + str(room_id),
            "password": str(room_password),
        }
        data_manager.set_data(current_rooms)

        # Log the successful creation of the room
        logger.info("Room {} created, password: {}.".format(room_id, password))

        # Return the room_id
        return room_id

    # End of segment if required, releases the recogniser and
    # initialises internal state.
    def reset(self):
        """
        Resets the connection and clears the state of the TranscriptionMasterBroadcastServer instance.

        This method performs the following actions:
        1. Logs a message indicating that the connection is being reset.
        2. If the instance is in MASTER mode, feeds an EOF (End-of-File) signal to the speech recognition engine.
        3. Closes the file descriptor (if it exists).
        4. Resets the instance variables.
        5. Cancels any ongoing timeout.
        6. Sets the state of the instance to WAITING.
        7. If the instance is in MASTER mode, removes the current room from the list of active rooms.

        Note: This method assumes that the instance variables `_reco`, `_fd`, and `_state` are defined.

        """
        logger.debug("Resetting connection")
        if self._fd is not None:
            self._fd.close()
            self._fd = None
        self.cancel_timeout()
        self._state = TranscriptionMasterBroadcastServer.WAITING
        if self.mode == TranscriptionMasterBroadcastServer.MASTER:
            data_manager = DataManager()
            current_rooms = data_manager.get_data()

            del current_rooms[self.room_id]

            data_manager.set_data(current_rooms)

    def send_systems(self):
        """
        Sends systems info to the client.

        This method retrieves the systems information from the TranscriptionMasterBroadcastServer
        and sends it to the client in the form of a JSON-encoded message.

        Returns:
            None
        """
        logger.info("Sending systems info to client")
        if TranscriptionMasterBroadcastServer.SYSCALL_CONTENT is None:
            tmp = [
                {"id": k, "lang": v.lang, "name": v.name, "cmllr": v.cmllr}
                for k, v in TranscriptionMasterBroadcastServer.RECOMNG.items()
            ]
            TranscriptionMasterBroadcastServer.SYSCALL_CONTENT = tmp
        msg = json.dumps(
            {
                "code": "SYSTEMS",
                "content": TranscriptionMasterBroadcastServer.SYSCALL_CONTENT,
            }
        ).encode("utf-8")
        self.sendMessage(msg)

    def send_rooms(self):
        """
        Sends rooms information to the client.

        This method retrieves the information of all the current rooms in the
        TranscriptionMasterBroadcastServer
        and sends it to the client in the form of a JSON-encoded message.

        Returns:
            None
        """
        logger.info("Sending rooms info to client")
        rooms_info = {}
        data_manager = DataManager()
        current_rooms = data_manager.get_data()

        for room_id, room_obj in current_rooms.items():
            rooms_info[room_id] = {
                "id": room_id,
                "name": room_obj["name"],
                "password": 1
                if "password" in room_obj and len(room_obj["password"]) > 0
                else 0,
            }
        msg = json.dumps({"code": "ROOMS", "content": rooms_info}).encode("utf-8")
        self.sendMessage(msg)

    def send_error(self, msg):
        """
        Sends an error message to the client.

        Args:
            msg (str): The error message to send.

        Returns:
            None
        """
        logger.info("Error sent to client: %s" % msg)
        self.sendMessage(json.dumps({"code": "ERR", "text": msg}).encode("utf-8"))
        if self._fd is not None:
            self._fd.close()
            self._fd = None
        self.cancel_timeout()
        self._state = TranscriptionMasterBroadcastServer.WAITING

    def send_ready(self):
        """
        Sends a 'READY' message to the client.

        If the server is in 'MASTER' mode, the message includes the room ID, room name,
        and room password.

        Returns:
            None
        """
        data_manager = DataManager()
        current_rooms = data_manager.get_data()
        logger.info("Master, sending ready to client...")
        self.sendMessage(
            json.dumps(
                {
                    "code": "READY",
                    "room_id": self.room_id,
                    "room_name": current_rooms[self.room_id]["name"],
                    "room_password": current_rooms[self.room_id]["password"],
                }
            ).encode("utf-8")
        )

    def send_hyp(self, novar, var, code):
        logger.debug("Sending hyp")
        ret = {"code": code, "text-novar": novar, "text-var": var}
        self.sendMessage(json.dumps(ret).encode("utf-8"))

    def send_end(self):
        logger.debug("Sending end")
        self.sendMessage(json.dumps({"code": "END"}).encode("utf-8"))

    def onConnect(self, request):
        self._id = None
        self._peer = request.peer
        logger.info("Client connecting: {}".format(request.peer))

    def onClose(self, wasClean, code, reason):
        try:
            logger.info("WebSocket connection closed: {}".format(reason))
            self.reset()
        except Exception as exc:
            logger.error(f"Exception on closed: {exc}", exc_info=True)

    def onOpen(self):
        self._id = "%d;%s" % (self.N, "{}".format(self._peer))
        self.N += 1
        self._state = self.WAITING
        self._fd = None
        self._timeout = None
        logger.info("Connection opened")

    def cancel_timeout(self):
        if self._timeout is not None:
            self._timeout.cancel()
            self._timeout = None

    def set_timeout(self):
        pass
        """
        def callback(obj):
             obj.reset()
             obj.msg('Timeout! Recognition finished')
        self.cancel_timeout()
        loop= asyncio.get_event_loop()
        if TIMEOUT>0:
            self._timeout= loop.call_later(TIMEOUT,callback,self)
        else: self._timeout= None
        """

    def onMessage(self, payload, isBinary):
        msg = json.loads(payload.decode("utf-8"))
        code = self.__read_code(payload, isBinary)

        logger.info(f"onMessage. Code: {code} Payload: {payload}")

        if code == "ROOM_LIST":
            self.send_rooms()
            return

        if (
            self._state == TranscriptionMasterBroadcastServer.WAITING
        ):  # Waiting for the OPEN code
            if not self.__read_open_code(payload, isBinary):
                return self.send_error("Unable to open connection")
                # self.msg('Discarded data: (%d,%s)'%(len(payload),isBinary))
                # return # Discard packages.
            if self.mode == TranscriptionMasterBroadcastServer.MASTER:
                logger.debug(
                    "Received message in MASTER mode (onMessage). Creating recogniser..."
                )
                self.room_id = self.create_room(
                    self.__read_channel(payload, isBinary),
                    self.__read_password(payload, isBinary),
                )
                self._state = TranscriptionMasterBroadcastServer.PROCESSING
                logger.info("Changing to master")
                self.send_ready()
            else:
                logger.info("Unknown mode ...")
                return self.send_error(("Server error"))

        elif (
            self._state == TranscriptionMasterBroadcastServer.PROCESSING
        ):  # Waiting for data or CLOSE
            code = self.__read_code(payload, isBinary)
            if code == "HYP" or code == "RES":  # Data
                logger.debug(f"Received code HYP or RES ({code})")
                self.cancel_timeout()
                msg = json.loads(payload.decode("utf-8"))

                sio.emit(
                    "new_message",
                    {
                        "channel": self.room_id,
                        "code": code,
                        "room_id": self.room_id,
                        "name": f"Meeting #{self.room_id}",
                        "text-novar": msg["text-novar"],
                        "text-var": msg["text-var"],
                    },
                )
                self.set_timeout()
            else:
                self.cancel_timeout()
                code = self.__read_code(payload, isBinary)
                if code == "CLOSE":
                    self.reset()
                    logger.info("Recognition finished")
                elif code == "ROOM_UPDATE":
                    # Modify room name / password
                    msg = json.loads(payload.decode("utf-8"))
                    data_manager = DataManager()
                    current_rooms = data_manager.get_data()
                    if "name" in msg:
                        current_rooms[self.room_id]["name"] = msg["name"]
                    if "password" in msg:
                        current_rooms[self.room_id]["password"] = msg["password"]
                    if "description" in msg:
                        current_rooms[self.room_id]["description"] = msg["description"]
                    logger.info("Room properties modified")
                    data_manager.set_data(current_rooms)
                    logger.debug("Current rooms (onMessage): {}".format(current_rooms))
                else:
                    return self.__wrong_protocol()
                self.set_timeout()

        elif self._state == TranscriptionMasterBroadcastServer.LISTENING:
            if code == "CLOSE":
                self.reset()
                self.send_end()
                ("Listening finished")
            elif code == "ROOM_LIST":
                self.send_rooms()
            else:
                return self.__wrong_protocol()

    # end onMessage
