from app.utils import logger


class Recogniser:
    def __free(self):
        logger.debug("Freeing Recogniser")
        self.last_word = None
        for s in self.servers:
            s.send_end()
        self.servers = []
        logger.info("END sent to client")

    def __init__(self):
        logger.debug("Initialising Recogniser")
        self.servers = []

    def set_server(self, server):
        logger.debug(f"Setting server: {server}")
        self.servers.append(server)
        logger.debug(f"Current servers: {self.servers}")

    def del_server(self, server):
        logger.debug(f"Deleting server: {server}")
        if server in self.servers:
            self.servers.remove(server)

    def reset_cmllr(self):
        self.reco.reset_cmllr()

    def discard_samples(self):
        self.discard = True

    def is_bad(self):
        return self.bad

    def process_out(self, code, novar, var):
        logger.info(f"Sending output to servers: {self.servers}")
        for s in self.servers:
            logger.debug(f"Sending output to server: {s}")
            s.send_hyp(novar, var, code)

    # None denotes end of segment
    def feed(self, data=None):
        pass

    def split(self):
        self.reco.split()
