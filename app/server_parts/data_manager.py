import pickle
from os import environ
from typing import Dict, Union

import redis
from dotenv import load_dotenv

from app.utils import logger

load_dotenv()
redis_url_with_pass = f"redis://default:{environ.get('REDIS_PASSWORD')}@{environ.get('REDIS_HOST')}:{environ.get('REDIS_PORT')}/{environ.get('REDIS_DB')}"

redis_connection = redis.Redis(
    host=environ.get("REDIS_HOST"),
    port=environ.get("REDIS_PORT"),
    password=environ.get("REDIS_PASSWORD"),
    db=0,
)


class DataManager:
    def __init__(self):
        self.redis_connection = redis_connection

    def get_rooms(self) -> dict:
        redis_rooms = self.redis_connection.get("rooms")
        if redis_rooms:
            rooms = pickle.loads(redis_rooms)
        else:
            rooms = {}
        return rooms

    def set_rooms(self, new_rooms: dict):
        pickled_rooms = pickle.dumps(new_rooms)
        self.redis_connection.set("rooms", pickled_rooms)

    def add_room(self, room_id: str, room_dict: dict) -> dict:
        logger.debug(f"Adding room {room_id}")
        rooms = self.get_rooms()
        rooms[room_id] = room_dict

        self.set_rooms(rooms)
        logger.debug(f"Rooms: {rooms}")

        return rooms[room_id]

    def delete_room(self, room_id: str) -> dict:
        logger.debug(f"Deleting room {room_id}")
        rooms = self.get_rooms()
        del rooms[room_id]

        self.set_rooms(rooms)
        logger.debug(f"Rooms: {rooms}")

        return rooms[room_id]

    def get_room(self, room_id: str) -> Union[Dict, None]:
        rooms = self.get_rooms()
        logger.debug(f"Rooms: {rooms}")
        if room_id in rooms:
            return rooms[room_id]
        return None

    def get_room_count(self) -> int:
        rooms = self.get_rooms()
        return len(rooms)
