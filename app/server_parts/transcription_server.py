import json
from typing import Dict

from autobahn.asyncio.websocket import WebSocketServerProtocol
from dotenv import load_dotenv

from app.server_parts.recogniser import Recogniser
from app.utils import logger

load_dotenv()


class TranscriptionBroadcastServer(WebSocketServerProtocol):
    N = 0

    # Estat
    WAITING = 0
    PROCESSING = 1
    LISTENING = 2

    # Mode

    MASTER = 0  # transcribe (retrieving audio data)
    SLAVE = 1  # listen (just sending room transcription)

    RECOMNG = None  # To be initialized

    ROOM_COUNT = 0
    # This would go to Redis
    CURRENT_ROOMS: Dict[
        int, Dict[str, str]
    ] = {}  # {room_id: {reco: Recogniser, name: str, password: str}}

    # Systems call content
    SYSCALL_CONTENT = None

    def __wrong_protocol(self):
        logger.warning(
            "Trying to communicate using an unknown protocol;"
            + " connection will be shutdown"
        )
        self.reset()
        self.sendClose()

    # Return True if format is ok.
    def __read_open_code(self, payload, isBinary):
        if isBinary:
            return False
        try:
            msg = json.loads(payload.decode("utf-8"))
            logger.info("Mesage: {}".format(msg))
            if msg["code"] == "OPEN":
                if "room_id" in msg:
                    # Room check
                    if (
                        not int(msg["room_id"])
                        in TranscriptionBroadcastServer.CURRENT_ROOMS
                    ):
                        logger.warning("Room ID #" + str(msg["room_id"]) + " not found")
                        raise Exception(
                            "Room ID #" + str(msg["room_id"]) + " not found"
                        )

                    # Password check
                    if (
                        "password"
                        in TranscriptionBroadcastServer.CURRENT_ROOMS[
                            int(msg["room_id"])
                        ]
                        and len(
                            TranscriptionBroadcastServer.CURRENT_ROOMS[
                                int(msg["room_id"])
                            ]["password"]
                        )
                        > 0
                    ):
                        if (
                            "room_password" not in msg
                            or str(msg["room_password"])
                            != TranscriptionBroadcastServer.CURRENT_ROOMS[
                                int(msg["room_id"])
                            ]["password"]
                        ):
                            logger.warning("Invalid room password")
                            raise Exception("Invalid room password")
                    self.room_id = int(msg.get("room_id"))
                    self.mode = TranscriptionBroadcastServer.SLAVE
                    logger.info("Room ID: {}".format(self.room_id))
                else:
                    self.mode = TranscriptionBroadcastServer.MASTER
            else:
                logger.info(msg["code"])
                return False
        except Exception as exc:
            logger.error(
                f"Error opening connection: missing parameters: {exc}", exc_info=True
            )
            return False
        return True

    # Return the code, if not present return None
    def __read_code(self, payload, isBinary):
        if isBinary:
            return None
        msg = json.loads(payload.decode("utf-8"))
        return msg.get("code")

    def __read_channel(self, payload, isBinary):
        if isBinary:
            return None
        msg = json.loads(payload.decode("utf-8"))
        return msg.get("channel")

    def __read_password(self, payload, isBinary):
        """
        Reads the password from the payload.

        Args:
            payload (bytes): The payload containing the password.
            isBinary (bool): Indicates whether the payload is binary or not.

        Returns:
            str: The password extracted from the payload, or None if the payload is binary.
        """
        if isBinary:
            return None
        msg = json.loads(payload.decode("utf-8"))
        return msg.get("password")

    def create_room(self, channel, password):
        """
        This method creates a new room in the TranscriptionBroadcastServer.

        Parameters:
        channel (str): The channel name which will be used as the room_id.
        password (str): The password for the room.

        Returns:
        room_id (str): The ID of the created room.
        """
        # Log the start of room creation
        logger.info("Creating room...")

        # Set the room_id to the provided channel name
        room_id = channel

        # Set the room_password to the provided password
        room_password = password

        # Increment the ROOM_COUNT of TranscriptionBroadcastServer
        TranscriptionBroadcastServer.ROOM_COUNT += 1

        # Add the new room to the CURRENT_ROOMS dictionary of TranscriptionBroadcastServer
        TranscriptionBroadcastServer.CURRENT_ROOMS[room_id] = {
            "reco": self._recogniser,
            "name": "Channel #" + str(room_id),
            "password": str(room_password),
        }

        # Log the successful creation of the room
        logger.info("Room {} created, password: {}.".format(room_id, password))
        logger.debug(
            "Current rooms: {}".format(TranscriptionBroadcastServer.CURRENT_ROOMS)
        )

        # Return the room_id
        return room_id

    # End of segment if required, releases the recogniser and
    # initialises internal state.
    def reset(self):
        """
        Resets the connection and clears the state of the TranscriptionBroadcastServer instance.

        This method performs the following actions:
        1. Logs a message indicating that the connection is being reset.
        2. If the instance is in MASTER mode, feeds an EOF (End-of-File) signal to the speech recognition engine.
        3. Closes the file descriptor (if it exists).
        4. Resets the instance variables.
        5. Cancels any ongoing timeout.
        6. Sets the state of the instance to WAITING.
        7. If the instance is in MASTER mode, removes the current room from the list of active rooms.

        Note: This method assumes that the instance variables `_recogniser`, `_fd`, and `_state` are defined.

        """
        logger.debug("Resetting connection")
        if (
            self._recogniser is not None
            and self.mode == TranscriptionBroadcastServer.MASTER
        ):
            logger.info("Feeding EOF")
            self._recogniser.feed()
        if self._fd is not None:
            self._fd.close()
            self._fd = None
        self._recogniser = None
        self.cancel_timeout()
        self._state = TranscriptionBroadcastServer.WAITING
        if self.mode == TranscriptionBroadcastServer.MASTER:
            del TranscriptionBroadcastServer.CURRENT_ROOMS[self.room_id]

    def send_systems(self):
        """
        Sends systems info to the client.

        This method retrieves the systems information from the TranscriptionBroadcastServer
        and sends it to the client in the form of a JSON-encoded message.

        Returns:
            None
        """
        logger.info("Sending systems info to client")
        if TranscriptionBroadcastServer.SYSCALL_CONTENT is None:
            tmp = [
                {"id": k, "lang": v.lang, "name": v.name, "cmllr": v.cmllr}
                for k, v in TranscriptionBroadcastServer.RECOMNG.items()
            ]
            TranscriptionBroadcastServer.SYSCALL_CONTENT = tmp
        msg = json.dumps(
            {"code": "SYSTEMS", "content": TranscriptionBroadcastServer.SYSCALL_CONTENT}
        ).encode("utf-8")
        self.sendMessage(msg)

    def send_rooms(self):
        """
        Sends rooms information to the client.

        This method retrieves the information of all the current rooms in the
        TranscriptionBroadcastServer
        and sends it to the client in the form of a JSON-encoded message.

        Returns:
            None
        """
        logger.info("Sending rooms info to client")
        rooms_info = {}
        for room_id, room_obj in TranscriptionBroadcastServer.CURRENT_ROOMS.items():
            rooms_info[room_id] = {
                "id": room_id,
                "name": room_obj["name"],
                "password": 1
                if "password" in room_obj and len(room_obj["password"]) > 0
                else 0,
            }
        msg = json.dumps({"code": "ROOMS", "content": rooms_info}).encode("utf-8")
        self.sendMessage(msg)

    def send_error(self, msg):
        """
        Sends an error message to the client.

        Args:
            msg (str): The error message to send.

        Returns:
            None
        """
        logger.info("Error sent to client: %s" % msg)
        self.sendMessage(json.dumps({"code": "ERR", "text": msg}).encode("utf-8"))
        if (
            self._recogniser is not None
            and self.mode == TranscriptionBroadcastServer.MASTER
        ):
            logger.info("Discarding samples")
            self._recogniser.discard_samples()
            if self._nbytes != 0:
                logger.info("Feeding EOF")
                self._recogniser.feed()
        if self._fd is not None:
            self._fd.close()
            self._fd = None
        self._recogniser = None
        self.cancel_timeout()
        self._state = TranscriptionBroadcastServer.WAITING

    def send_ready(self):
        """
        Sends a 'READY' message to the client.

        If the server is in 'MASTER' mode, the message includes the room ID, room name,
        and room password.
        If the server is in 'SLAVE' mode, the message includes only the room ID and room name.

        Returns:
            None
        """
        if self.mode == TranscriptionBroadcastServer.MASTER:
            logger.info("Master, sending ready to client...")
            self.sendMessage(
                json.dumps(
                    {
                        "code": "READY",
                        "room_id": self.room_id,
                        "room_name": TranscriptionBroadcastServer.CURRENT_ROOMS[
                            self.room_id
                        ]["name"],
                        "room_password": TranscriptionBroadcastServer.CURRENT_ROOMS[
                            self.room_id
                        ]["password"],
                    }
                ).encode("utf-8")
            )
        else:
            logger.info("Slave, sending ready to client...")
            self.sendMessage(
                json.dumps(
                    {
                        "code": "READY",
                        "room_id": self.room_id,
                        "room_name": TranscriptionBroadcastServer.CURRENT_ROOMS[
                            self.room_id
                        ]["name"],
                    }
                ).encode("utf-8")
            )

    def send_hyp(self, novar, var, code):
        ret = {"code": code, "text-novar": novar, "text-var": var}
        self.sendMessage(json.dumps(ret).encode("utf-8"))

    def send_end(self):
        self.sendMessage(json.dumps({"code": "END"}).encode("utf-8"))

    def onConnect(self, request):
        self._id = None
        self._peer = request.peer
        logger.info("Client connecting: {}".format(request.peer))

    def onClose(self, wasClean, code, reason):
        try:
            logger.info("WebSocket connection closed: {}".format(reason))
            self.reset()
        except Exception as exc:
            logger.error(f"Exception on closed: {exc}", exc_info=True)

    def onOpen(self):
        self._id = "%d;%s" % (self.N, "{}".format(self._peer))
        self.N += 1
        self._state = self.WAITING
        self._recogniser = None
        self._fd = None
        self._timeout = None
        logger.info("Connection opened")

    def cancel_timeout(self):
        if self._timeout is not None:
            self._timeout.cancel()
            self._timeout = None

    def set_timeout(self):
        pass
        """
        def callback(obj):
             obj.reset()
             obj.msg('Timeout! Recognition finished')
        self.cancel_timeout()
        loop= asyncio.get_event_loop()
        if TIMEOUT>0:
            self._timeout= loop.call_later(TIMEOUT,callback,self)
        else: self._timeout= None
        """

    def onMessage(self, payload, isBinary):
        msg = json.loads(payload.decode("utf-8"))
        code = self.__read_code(payload, isBinary)
        logger.info("MSG code: {}".format(code))
        logger.info("MSG: {}".format(payload))
        if code == "ROOM_LIST":
            self.send_rooms()
            return

        if (
            self._state == TranscriptionBroadcastServer.WAITING
        ):  # Waiting for the OPEN code
            if not self.__read_open_code(payload, isBinary):
                return self.send_error("Unable to open connection")
                # self.msg('Discarded data: (%d,%s)'%(len(payload),isBinary))
                # return # Discard packages.
            if self.mode == TranscriptionBroadcastServer.MASTER:
                self._recogniser = Recogniser()
                self.room_id = self.create_room(
                    int(self.__read_channel(payload, isBinary)),
                    int(self.__read_password(payload, isBinary)),
                )
                self._state = TranscriptionBroadcastServer.PROCESSING
                logger.info("Changing to master")
                self.send_ready()
            elif self.mode == TranscriptionBroadcastServer.SLAVE:
                self._recogniser = TranscriptionBroadcastServer.CURRENT_ROOMS[
                    self.room_id
                ]["reco"]
                self._recogniser.set_server(self)
                logger.info("Client listening...")
                self._state = TranscriptionBroadcastServer.LISTENING
                self.send_ready()
            else:
                logger.info("Unknown mode ...")
                return self.send_error(("Server error"))

        elif (
            self._state == TranscriptionBroadcastServer.PROCESSING
        ):  # Waiting for data or CLOSE
            code = self.__read_code(payload, isBinary)
            if code == "HYP" or code == "RES":  # Data
                self.cancel_timeout()
                msg = json.loads(payload.decode("utf-8"))
                self._recogniser.process_out(code, msg["text-novar"], msg["text-var"])
                self.set_timeout()
            else:
                self.cancel_timeout()
                code = self.__read_code(payload, isBinary)
                if code == "CLOSE":
                    self.reset()
                    logger.info("Recognition finished")
                elif code == "ROOM_UPDATE":
                    # Modify room name / password
                    msg = json.loads(payload.decode("utf-8"))
                    if "name" in msg:
                        TranscriptionBroadcastServer.CURRENT_ROOMS[self.room_id][
                            "name"
                        ] = msg["name"]
                    if "password" in msg:
                        TranscriptionBroadcastServer.CURRENT_ROOMS[self.room_id][
                            "password"
                        ] = msg["password"]
                    if "description" in msg:
                        TranscriptionBroadcastServer.CURRENT_ROOMS[self.room_id][
                            "description"
                        ] = msg["description"]
                    logger.info("Room properties modified")
                else:
                    return self.__wrong_protocol()
                self.set_timeout()

        elif self._state == TranscriptionBroadcastServer.LISTENING:
            if code == "CLOSE":
                self._recogniser.del_server(self)
                self.reset()
                self.send_end()
                ("Listening finished")
            elif code == "ROOM_LIST":
                self.send_rooms()
            else:
                return self.__wrong_protocol()

    # end onMessage
